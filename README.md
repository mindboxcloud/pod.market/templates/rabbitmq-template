# openshift-rabbitmq
This is working version of newest Rabbitmq server on newest erlang platform. This version is designed to run single node (in this moment). It provides access to admin panel on `15672` port.

To build: `docker build -t rabbitmq .`

To start it you can do: `docker run -it -p 80:15672 -p 5672:5672 rabbitmq`

Work further will enable cluster mode.
