FROM registry.access.redhat.com/rhel7/rhel

LABEL maintainer="R&D Mindbox <ran2d@mindboxgroup.com>"
LABEL io.k8s.description="Lightweight open source message broker" \
      io.k8s.display-name="RabbitMQ" \
      io.openshift.expose-services="4369:epmd, 5671:amqp, 5672:amqp, 25672:http, 15671:http-management 15672:https-management" \
      io.openshift.tags="rabbitmq"

ENV RABBITMQ_VERSION 3.7.11
ENV ERLANG_VERSION 21.2.4

RUN yum -y install --enablerepo rhel-server-rhscl-7-eus-rpms nss_wrapper gettext && yum clean all
RUN yum install -y hostname https://github.com/rabbitmq/erlang-rpm/releases/download/v${ERLANG_VERSION}/erlang-${ERLANG_VERSION}-1.el7.centos.x86_64.rpm && yum clean all
RUN yum install -y https://github.com/rabbitmq/rabbitmq-server/releases/download/v${RABBITMQ_VERSION}/rabbitmq-server-${RABBITMQ_VERSION}-1.el7.noarch.rpm && yum clean all
