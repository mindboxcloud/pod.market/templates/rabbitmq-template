#!/bin/bash

LOCK_FILE=/var/lib/rabbitmq/.default_user_lock


if [ ! -f $LOCK_FILE ]; then

   # Wait for RabbitMQ startup
   for (( ; ; )) ; do
     sleep 5
     rabbitmqctl -q node_health_check > /dev/null 2>&1
     if [ $? -eq 0 ] ; then
       echo "$0 `date` rabbitmq is now running"
       break
     else
       echo "$0 `date` waiting for rabbitmq startup"
     fi
   done
   
   rabbitmqctl add_user $RABBITMQ_WORK_USER $RABBITMQ_WORK_PASS
   rabbitmqctl set_permissions -p $RABBITMQ_VHOST $RABBITMQ_WORK_USER ".*" ".*" ".*"
   touch $LOCK_FILE
fi
