#!/bin/bash
set -eu

export USER_ID=$(id -u)
export GROUP_ID=$(id -g)
envsubst < /usr/local/bin/passwd.template > /tmp/passwd
export LD_PRELOAD=libnss_wrapper.so
export NSS_WRAPPER_PASSWD=/tmp/passwd
export NSS_WRAPPER_GROUP=/etc/group

RABBITMQ_HOST=${1:-"localhost"}
CLUSTER_SIZE=${2:-"2"}
RABBITMQ_NODE=${3:-"rabbit-cluster-0.rabbit-cluster"}

if [ "${RABBITMQ_ERLANG_COOKIE:-}" ]; then
	cookieFile='/var/lib/rabbitmq/.erlang.cookie'
	if [ -e "$cookieFile" ]; then
		if [ "$(cat "$cookieFile" 2>/dev/null)" != "$RABBITMQ_ERLANG_COOKIE" ]; then
			echo >&2
			echo >&2 "warning: $cookieFile contents do not match RABBITMQ_ERLANG_COOKIE"
			echo >&2
		fi
	else
		echo "$RABBITMQ_ERLANG_COOKIE" > "$cookieFile"
	fi
        chown ${USER_ID} "$cookieFile"
        chmod 600 "$cookieFile"
fi

LOCK_FILE=/etc/rabbitmq/.default_user_lock

if [ ! -f $LOCK_FILE ]; then

   # waiting for all cluster nodes will be running
   /usr/local/bin/rmq-watcher.py $RABBITMQ_HOST $RABBITMQ_ADMIN_USER $RABBITMQ_ADMIN_PASS $CLUSTER_SIZE

   rabbitmqctl -n rabbit@$RABBITMQ_NODE add_user $RABBITMQ_WORK_USER $RABBITMQ_WORK_PASS
   rabbitmqctl -n rabbit@$RABBITMQ_NODE set_permissions -p $RABBITMQ_VHOST $RABBITMQ_WORK_USER ".*" ".*" ".*"
   touch $LOCK_FILE
fi

exec "$@"
