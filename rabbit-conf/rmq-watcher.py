#!/bin/python

import time
import sys
import json
import urllib2
import base64

host=sys.argv[1]
user=sys.argv[2]
password=sys.argv[3]
cluster_size=int(sys.argv[4])

print("Arguments: host={} user={} cluster_size={}".format(host, user, cluster_size))

url = 'http://{}:15672/api/nodes?columns=name,running'.format(host)
request = urllib2.Request(url)
b64auth = base64.standard_b64encode("%s:%s" % (user,password))
request.add_header("Authorization", "Basic %s" % b64auth)

time.sleep(15)
available=False
while not available:
   try:
     print("Trying to obtain cluster informations")
     result = urllib2.urlopen(request)
     data = result.read()
     data = json.loads(data)
     print("Data:", data)

     ready_nodes = sum([1 for node in data if node['running']])
     print("Ready Nodes: {}".format(ready_nodes))
     print("Requested cluster size: {}".format(cluster_size))
     if ready_nodes == cluster_size:
        print("Cluster Available")
        available=True
   except Exception as e:
     print("Problem ocured")
     print(e)
     pass
