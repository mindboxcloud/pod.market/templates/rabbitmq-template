#!/bin/bash
set -eux

export USER_ID=$(id -u)
export GROUP_ID=$(id -g)
envsubst < /usr/local/bin/passwd.template > /tmp/passwd
export LD_PRELOAD=libnss_wrapper.so
export NSS_WRAPPER_PASSWD=/tmp/passwd
export NSS_WRAPPER_GROUP=/etc/group

if [ "${RABBITMQ_ERLANG_COOKIE:-}" ]; then
	cookieFile='/var/lib/rabbitmq/.erlang.cookie'
	if [ -e "$cookieFile" ]; then
		if [ "$(cat "$cookieFile" 2>/dev/null)" != "$RABBITMQ_ERLANG_COOKIE" ]; then
			echo >&2
			echo >&2 "warning: $cookieFile contents do not match RABBITMQ_ERLANG_COOKIE"
			echo >&2
		fi
	else
		echo "$RABBITMQ_ERLANG_COOKIE" > "$cookieFile"
	fi
        chown ${USER_ID} "$cookieFile"
        chmod 600 "$cookieFile"
fi

if [ -e "$RABBITMQ_CONFIG_FILE" ]; then
  HOST_PREFIX="cluster_formation.k8s.hostname_suffix = .$K8S_SERVICE_NAME.$POD_NAMESPACE.svc.cluster.local"
  grep -qF -- "$HOST_PREFIX" "$RABBITMQ_CONFIG_FILE" || echo "$HOST_PREFIX" >> "$RABBITMQ_CONFIG_FILE"
fi

exec "$@"
